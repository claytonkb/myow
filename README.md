# myow

Make Your Own Wizard

![MYOW](img/myow_small.png)

Unless you installed software that uses Perl's YAML and Hash::Merge packages
(possible but unlikely), you will need to install them before running this
script. Open a terminal and type the following commands:

    sudo cpan -if install YAML
    sudo cpan -if install Hash::Merge

If you run on a different distro than Linux Mint, there may be additional
packages you need to install. If so, Perl will complain and you can just run
the same command as above, just edited to install whatever missing package Perl
is complaining about.

After you have installed the required packages, you can run the example wizard
with the following command (from a terminal, in whatever directory where the
myow.pl script is at):

    perl myow.pl examples/how_to_write_a_wizard.yaml

Enter 'c' or 'i' to move forward through the wizard, and enter 'q' to exit. At
any step in the wizard, press 'p' for more information about the current step.

For more information about switches, just type:

    perl myow.pl

And that's all there is to it.

If you want to write your own wizard, just copy the example .yaml file and
rename it to whatever you want (also, you can place it anywhere, it doesn't
have to be in the examples directory) and start editing. With YAML, you have to
be careful to keep your indentation consistent. Don't use tab-characters to
align text (you'll get a syntax error), just use spaces. Refer to this link for
more information on YAML than you will ever need to write a config file for the
MYOW wizard: https://learnxinyminutes.com/docs/yaml

Note that the MYOW tool is intended to help you document any multi-step task
that you repeat occasionally (thus, you don't have the steps memorized.) It
doesn't have to be computer-related. It could be mailing out a newsletter,
downloading your bank statements, or even rebuilding an engine.  This is how I
will be using this tool and that's why I wrote it -- over the long run, I
expect it to save me a lot more time than it took to write. The advantage to a
wizard versus just writing a plain text file or office document is that it
helps you focus on one step at a time (by only showing the current step) and it
can also remember your progress if you partially complete a task and want to
resume it later on (see the -o switch and the 's' command in the wizard).


