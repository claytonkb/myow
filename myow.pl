#!/usr/bin/perl

# MYOW -- Make Your Own Wizard
#
# Usage: perl myow.pl [options] <wizard.yaml>
#           To see usage message, run myow.pl with no commandline arguments

use strict;

use File::Basename;
use YAML;
use Data::Dumper;
use Getopt::Long;
use IPC::Cmd qw[can_run run run_forked];
use Hash::Merge;
my $merge = Hash::Merge->new();

my  $cl_options;
    $cl_options->{yaml_name};
    $cl_options->{a};
    $cl_options->{f};
    $cl_options->{o};
    $cl_options->{s};

GetOptions( "a"   => \$cl_options->{a},
            "f"   => \$cl_options->{f},
            "o=s" => \$cl_options->{o},
            "s"   => \$cl_options->{s});

$cl_options->{yaml_name} = shift(@ARGV) if($#ARGV > -1);

help() and die unless(defined $cl_options->{yaml_name});

my $options = init_myow($cl_options);

print_wizard_name($options);
run_wizard($options);

sub run_wizard{

    my $steps = $options->{wizard_yaml}{wizard_steps};

    my $step_num = 0;

    my $commands  = $options->{commands};

    my $steps_displayed = 0;

    while($step_num <= $#{$steps}){

        if(!defined $options->{cl_options}{a}
                and
            $steps->[$step_num]{completed} eq "Y"){
                print_completed($step_num);
                $step_num++;
                next;
        }

        if($steps_displayed < ($step_num+1)){
            print_step($options, $steps, $step_num);
            $steps_displayed=$step_num+1;
        }

        my $user_choice;

        my $step_first_time = 1;

        do{

            print $options->{wizard_yaml}{wizard_prompt} . " ";
            $user_choice = <>;
            chomp($user_choice);

        } while($user_choice !~ /^$commands$/);

        last if($user_choice eq "q");

        if($user_choice eq "p"){

            print "$steps->[$step_num]{procedure}\n";

        }
        elsif($user_choice eq "c"){

            $steps->[$step_num]{completed} = "Y";
            print_completed($step_num);
            $step_num++;

        }
        elsif($user_choice eq "r"){

            unless(exists $steps->[$step_num]{runnable} 
                        and 
                   $steps->[$step_num]{runnable} eq "Y"){

                print "Not runnable\n";

            }
            else{

                my @commands = split(/\n/, $steps->[$step_num]{procedure});

                for(@commands){
                    my $ret_val =  myow_do($_);
                    print "$ret_val->[0]";
                }

            }

        }
        elsif($user_choice eq "i"){

            print_incomplete($step_num);
            $step_num++;

        }
        elsif($user_choice eq "s"){

            if( defined $options->{cl_options}{o} ){

                print "Saving progress to $options->{cl_options}{o} ...\n";
                save_yaml_file($options->{cl_options}{o}, $options->{wizard_yaml});

            }
            else{

                print "Please enter the filename to save your changes\n";
                print ": ";

                $user_choice = <>;
                chomp($user_choice);

                print "Saving progress to $user_choice ...\n";
                save_yaml_file($user_choice, $options->{wizard_yaml});

            }
            return;

        }
        else{

            $step_num++;

        }

    }

    if( defined $options->{cl_options}{o} ){
        print "Saving progress to $options->{cl_options}{o} ...\n";
        save_yaml_file($options->{cl_options}{o}, $options->{wizard_yaml});
    }

}


sub print_incomplete{
    my $step_num = shift;
    print "\nStep " . ($step_num+1) . " INCOMPLETE\n\n";
}


sub print_completed{
    my $step_num = shift;
    print "\nStep " . ($step_num+1) . " COMPLETED\n\n";
}


sub print_step{

    my $options = shift;
    my $steps = shift;
    my $step_num = shift;

    print "Step " . ($step_num+1) . " of " . ($#{$steps}+1) . ": ";
    print "$steps->[$step_num]{step}\n";
    print "$options->{info_line}\n";

}


sub print_wizard_name{

    my $options = shift;

    my $heavy_bar = "##############################################################################";

    print "\n$heavy_bar\n";
    print "    $options->{wizard_yaml}{wizard_name}\n";
    print "$heavy_bar\n\n";

}


sub init_myow{

    my ($cl_options) = @_;

    my $options = {};

    my $wizard_yaml_defaults = {
        wizard_name   => "MYOW",
        wizard_prompt => ">",
    };

    if($cl_options->{s}){ # sequential / strict mode
        $options->{info_line} = "(c-completed, p-procedure, r-run, s-save and quit, q-quit)";
        $options->{commands} = "[cprsq]";
    }
    else{
        $options->{info_line} = "(c-completed, i-incomplete, p-procedure, r-run, s-save and quit, q-quit)";
        $options->{commands} = "[ciprsq]";
    }

    $options->{wizard_yaml} = {};
    my $wizard_yaml         = load_yaml_file($cl_options->{yaml_name});
    $options->{wizard_yaml} = $merge->merge($options->{wizard_yaml}, $wizard_yaml);
    $options->{wizard_yaml} = $merge->merge($options->{wizard_yaml}, $wizard_yaml_defaults);

    $options->{cl_options}  = $cl_options;

    return $options;

}


sub help{

print <<USAGE;
USAGE: perl myow.pl [options] <wizard.yaml>
    options
    -a          display all steps (including completed steps)
    -f          don't prompt re. saving output file (force-quit when done)
    -s          sequential / strict mode -- steps cannot be marked incomplete
    -o <file>   save resulting YAML wizard out to <file>
USAGE

}


sub save_yaml_file{
    my $file_name = shift;
    my $yaml_object = shift;
    my $text = to_yaml($yaml_object);
    open YAML_FILE, ">$file_name" or die "Couldn't open $file_name $!\n";
    print YAML_FILE $text;
    close YAML_FILE;
}


sub load_yaml_file{
    my $file_name = shift;
    open YAML_FILE, $file_name or die "Couldn't open $file_name $!\n";
    my $text = join('', <YAML_FILE>);
    close YAML_FILE;
    return Load($text);
}


sub to_yaml{
    return YAML::Dump(shift);
}


sub myow_do {

    my $cmd = shift;

    my ($success, $error_message, $full_buf, $stdout_buf, $stderr_buf  ) = 
        run( command => $cmd, verbose => 0 );    

    if(!$success){
        myow_say("Failure detected, stopping...");
        print "$error_message\n";
        print "@{$stderr_buf}\n";
        die;
    }

    return $stdout_buf;

}


sub myow_say {
    print "MYOW.PL: $_[0]\n";
}


